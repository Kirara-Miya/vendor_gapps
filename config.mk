#
# Copyright (C) 2022 The Project Nyanpasu
#
# SPDX-License-Identifier: Apache-2.0
#

# Android Build system uncompresses DEX files (classes*.dex) in the privileged
# apps by default, which breaks APK v2/v3 signature. Because some privileged
# GMS apps are still shipped with compressed DEX, we need to disable
# uncompression feature to make them work correctly.
DONT_UNCOMPRESS_PRIV_APPS_DEXS := true

PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/app/GoogleExtShared/GoogleExtShared.apk \
    system/app/GoogleExtShared/oat/% \
    system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk \
    system/priv-app/GooglePackageInstaller/oat/% \
    system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk \
    system/app/GooglePrintRecommendationService/oat/% \
    system/etc/permissions/privapp-permissions-google-system.xml \
    system/etc/sysconfig/google-hiddenapi-package-allowlist.xml

# Permission
# System
PRODUCT_PACKAGES += \
    privapp-permissions-google-system.xml \
    google-hiddenapi-package-allowlist.xml

# Product
PRODUCT_PACKAGES += \
    default-permissions-google.xml \
    com.google.android.dialer.support.xml \
    privapp-permissions-google-comms-suite.xml \
    privapp-permissions-google-product.xml \
    split-permissions-google.xml \
    google.xml \
    play_store_fsi_cert.der \
    gms_fsverity_cert.der \
    game_service.xml \
    google_allowlist.xml \
    pixel_2016_exclusive.xml \
    wellbeing.xml

# System_ext
PRODUCT_PACKAGES += \
    privapp-permissions-google-system-ext.xml
